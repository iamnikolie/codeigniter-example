<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>CodeIgniter Example</title>

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/2.1.5/jquery.fancybox.min.css">

    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">

    <link rel="stylesheet" href="<?=asset_url() ?>css/main.css">

</head>
<body>

<nav class="navbar navbar-default">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">CodeIgniter Example</a>
        </div>
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
                <li class="active"><a href="#">Home</a></li>
            </ul>
            <ul class="nav navbar-nav navbar-right">
                <li><a href="http://www.nature.com/nature/index.html">Look at some nature</a></li>
            </ul>
        </div>
    </div>
</nav>

<div class="container">
    <div class="row">
        <div class="col-md-6 col-md-offset-3">

            <div class="panel panel-default">
                <div class="panel-heading text-center">
                    <h3>Some heading</h3>
                </div>
                <div class="panel-body">
                    <form action="index.php/form_example/do_stuff/" method="POST">
                        <div class="form-group">
                            <div class="col-md-4">
                                <label for="first_name" class="control-label">First name</label>
                            </div>
                            <div class="col-md-8">
                                <input type="text" name="first_name" required class="form-control">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-4">
                                <label for="last_name" class="control-label">Last name</label>
                            </div>
                            <div class="col-md-8">
                                <input type="text" name="last_name" required class="form-control">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-4">
                                <label for="age" class="control-label">Age</label>
                            </div>
                            <div class="col-md-8">
                                <input type="number" min="1" max="150" name="age" class="form-control">
                            </div>
                        </div>
                        <div>
                            <div class="col-md-4">
                                <label for="birth_date" class="control-label">Birth date</label>
                            </div>
                            <div class="col-md-8">
                                <input type="text" name="birth_date" class="birth_date form-control">
                            </div>
                        </div>
                        <div>
                            <div class="col-md-4">
                            </div>
                            <div class="col-md-8">
                                <input type="submit" name="submit" class="btn btn-success">
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <table class="table table-bordered">
                <tr>
                    <th>First name</th>
                    <th>Last name</th>
                    <th>Age</th>
                    <th>Date</th>
                </tr>
                <?php foreach($someData as $item): ?>
                    <tr>
                        <td><?=$item->first_name?></td>
                        <td><?=$item->last_name?></td>
                        <td><?=$item->age?></td>
                        <td><?=$item->birth_date?></td>
                    </tr>
                <?php endforeach; ?>
            </table>
        </div>
    </div>
</div>

<script type="text/javascript" src="https://code.jquery.com/jquery-2.2.0.min.js"></script>
<script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
<script type="text/javascript" src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/fancybox/2.1.5/jquery.fancybox.min.js"></script>
<script type="text/javascript" src="<?=asset_url() ?>js/common.js"></script>



</body>
</html>