<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ExampleForm_Model extends CI_Model {

    var $first_name   = '';
    var $last_name = '';
    var $age = '';
    var $birth_date = '';

    function __construct()
    {
        parent::__construct();
    }

    function insert($data)
    {
        $this->first_name   = $data['first_name'];
        $this->last_name = $data['last_name'];
        $this->age = $data['age'];
        $this->birth_date = $data['birth_date'];

        $this->db->insert('data', $this);
    }

    function get()
    {
        $query = $this->db->get('data');
        return $query->result();
    }

}